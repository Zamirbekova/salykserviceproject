package com.example.demo.pagination;

import com.example.demo.model.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class UserPaginationResponse {
    private int pages;
    private int currentPages;
    private List<User> findAll;

}
