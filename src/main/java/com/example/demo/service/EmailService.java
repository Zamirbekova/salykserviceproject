package com.example.demo.service;

import javax.mail.MessagingException;

public interface EmailService {
     void send(String to, String message);
     void sendHtmlMessage(String to, String htmlMessage) throws MessagingException;
}
