package com.example.demo.service;

import com.example.demo.exception.BadRequestException;
import com.example.demo.model.User;
import com.example.demo.pagination.UserPaginationResponse;

import java.util.List;

public interface UserService {
    User register(User user) throws BadRequestException;
    User findById(Long id);
    void delete(Long id);
    List<User> findAll();
     UserPaginationResponse paginationResponse(int page, int size);
     User getById(Long id);
}
