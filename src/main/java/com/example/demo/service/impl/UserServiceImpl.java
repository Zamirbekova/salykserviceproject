package com.example.demo.service.impl;

import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.User;
import com.example.demo.pagination.UserPaginationResponse;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.EmailService;
import com.example.demo.service.UserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final EmailService emailService;
   private final PasswordEncoder passwordEncoder;
   private final ModelMapper modelMapper;
    @Override
public User register(User user) throws BadRequestException {

    String email = user.getEmail();

        if (repository.existsByEmail(email)) {
        throw new BadRequestException("user with email already in use");
    }

    String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);


    User saveUser = repository.save(user);

    String userEmail = saveUser.getEmail();

    String message = "You successfully registered to Salyk-Service";

        try {
        emailService.sendHtmlMessage(userEmail, message);
    } catch (Exception e) {}

        return modelMapper.map(saveUser, User.class);

}


    @Override
    public User findById(Long id) {
        return repository.findById(id).orElseThrow(()->new NotFoundException("not found"));
    }

    @Override
    public void delete(Long id) {
repository.deleteById(id);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }
    @Override
    public UserPaginationResponse paginationResponse(int page,int size){
        Pageable pageable = PageRequest.of(page, size, Sort.by("studentName"));
        UserPaginationResponse userPaginationResponse = new UserPaginationResponse();
        userPaginationResponse.setPages((repository.findAll(pageable).getTotalPages()));
        userPaginationResponse.setCurrentPages(pageable.getPageNumber());
        userPaginationResponse.setFindAll(repository.findAll(pageable).getContent());
        return userPaginationResponse;

    }

    @Override
    public User getById(Long id) {
        return repository.getOne(id);
    }
}
