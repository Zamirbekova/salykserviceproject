package com.example.demo.service.impl;

import com.example.demo.service.EmailService;
import lombok.AllArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@AllArgsConstructor
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender javaMailSender;
@Override
    public void send(String to, String message) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("mailfromspringboot0@gmail.com");
        simpleMailMessage.setSubject("test");
        simpleMailMessage.setTo(to);
        simpleMailMessage.setText(message);
        this.javaMailSender.send(simpleMailMessage);
    }
@Override
    public void sendHtmlMessage(String to, String htmlMessage) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(
                mimeMessage,
                true,
                "UTF-8"
        );
        mimeMessageHelper.setTo(to);
        mimeMessageHelper.setSubject("hacker");
        mimeMessageHelper.setText(htmlMessage, true);
        javaMailSender.send(mimeMessage);
    }
}