package com.example.demo.api;

import com.example.demo.exception.BadRequestException;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserApi {
    private final UserService service;
    @GetMapping("/new")
    public String newUser(Model model){
        model.addAttribute("modelNewUser",new User());
        return "VIEW/add";
    }
    @PostMapping("/save")
    public String saveUser(@ModelAttribute("modelNewUser") User user) throws BadRequestException {
        service.register(user);
        return "redirect:/users";
    }
    @GetMapping("/getAll")
    public String getAllUsers(Model model){
        model.addAttribute("modelGetAllUsers",service.findAll());
        return "VIEW/getAllUsers";

    }
    @DeleteMapping("/{id}")
    public String deleteStudent(@PathVariable("id") Long id){
        service.delete(id);
        return "redirect:/users";
    }
    @GetMapping("/{id}")
    public String getStudentById(@PathVariable("id") Long id, Model model){
        model.addAttribute("modelUserByID",service.getById(id));
        return "VIEW/getById";
    }
}
