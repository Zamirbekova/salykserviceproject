package com.example.demo.model;

import com.example.demo.annotation.ValidPassword;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_sequence")
    @SequenceGenerator(name = "users_sequence", sequenceName = "users_sequence", allocationSize = 1)
    private Long id;
    @NotBlank
    private String fullName;
    @Email
    private String email;
    @NotBlank
    private String login;
    @ValidPassword
    private String password;

}
